import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

export default function CourseCard() {
  return (
    <Card>
      <Card.Body>
        <Card.Title>Sample Course</Card.Title>
        <Card.Text>
          Description: 
        </Card.Text>
        <Card.Text>
          This is a sample course offering.
        </Card.Text>
        <Card.Text>
          Price:
        </Card.Text>
        <Card.Text>
          PHP 40,000
        </Card.Text>
        
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}
