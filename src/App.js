import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import {Container} from 'react-bootstrap';

import './App.css';


function App() {
  return (
    // <> </> common pattern in React for components to return multiple lements
    <>
      < AppNavBar/>
      <Container>
      < Home />
      </Container>
    </>
  );
}

export default App;
