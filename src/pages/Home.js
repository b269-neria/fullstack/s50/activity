import Banner from '../components/Banner';
import Highlights from '../components/Highlight';
import CourseCard from '../components/CourseCard';

export default function Home() {
	return (
		<>
	< Banner/>
    < Highlights/>
    < CourseCard />
		</>
		)
}